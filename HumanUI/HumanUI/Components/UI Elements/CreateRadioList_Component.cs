﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace HumanUI.Components.UI_Elements
{
    /// <summary>
    /// A component to create a scrollviewer/listbox containing multiple checkbox items. 
    /// </summary>
    /// <seealso cref="Grasshopper.Kernel.GH_Component" />
    public class CreateRadioList_Component : GH_Component
    {
        /// <summary>
        /// Initializes a new instance of the CreateCheckList_Component class.
        /// </summary>
        public CreateRadioList_Component()
            : base("Create Radio List", "RadioList",
                "Creates a listbox containing radio buttons.",
                "Human UI", "UI Elements")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddTextParameter("Radio Items", "L", "The initial list of options to display in the checklist.", GH_ParamAccess.list);
            pManager.AddIntegerParameter("Index", "i", "The initially selected index. Defaults to 0.", GH_ParamAccess.item, 0);
            pManager.AddNumberParameter("Height", "H", "Optional checklist box height in pixels.", GH_ParamAccess.item);
            pManager[2].Optional = true;

        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("RadioList", "RL", "The RadioList object", GH_ParamAccess.item);
        }

        public override GH_Exposure Exposure => GH_Exposure.primary;

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            List<string> listItems = new List<string>();
            var index = 0;
            double height = 100;
            if (!DA.GetDataList<string>("Radio Items", listItems)) return;

            DA.GetData<int>("Index", ref index);
            //initialize the scroll viewer
            var sv = new ScrollViewer
            {
                CanContentScroll = true,
                VerticalScrollBarVisibility = ScrollBarVisibility.Auto
            };
            //initialize the itemsControl
            var ic = new ItemsControl();

            if (DA.GetData<double>("Height", ref height))
            {
                //set height of the scrollViewer
                sv.Height = height;
            }
            //for all the items in the list
            var group = Guid.NewGuid();
            if (0 > index || index > listItems.Count - 1) index = 0;
            for (var i = 0; i < listItems.Count; i++)
            {
                //create a new checkbox, and add it to the items control
                var item = listItems[i];
                var rb = new RadioButton
                {
                    Margin = new System.Windows.Thickness(2),
                    Content = item,
                    IsChecked = index == i,
                    GroupName = group.ToString("N")
                };
                ic.Items.Add(rb);
            }
            
            //put the items control into the scrollviewer
            sv.Content = ic;

            //pass out the scrollviewer
            DA.SetData("RadioList", new UIElement_Goo(sv, "RadioList", InstanceGuid, DA.Iteration));

        }

        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        // protected override System.Drawing.Bitmap Icon => Properties.Resources.createChecklist;

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid => new Guid("{3E21AC2C-C002-4F25-A7BB-776B5F90F0D3}");
    }
}