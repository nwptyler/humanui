﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Grasshopper.Kernel;

namespace HumanUI.Components.UI_Output
{
    /// <summary>
    /// A component to set the contents of an existing Checklist object
    /// </summary>
    /// <seealso cref="Grasshopper.Kernel.GH_Component" />
    public class SetRadioList_Component : GH_Component
    {
        /// <summary>
        /// Initializes a new instance of the SetList_Component class.
        /// </summary>
        public SetRadioList_Component()
            : base("Set RadioList Contents", "SetRadioList",
                "Use this to set the contents of a radio list",
                "Human UI", "UI Output")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("RadioList to modify", "L", "The list object to modify", GH_ParamAccess.item);
            pManager[pManager.AddTextParameter("New radiolist contents", "C", "The new items to display in the checklist", GH_ParamAccess.list)].Optional = true;
            pManager[pManager.AddIntegerParameter("Selected Index", "I", "The initially selected index. Defaults to the first item.", GH_ParamAccess.item, 0)].Optional = true; ;
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            object listObject = null;
            List<string> listContents = new List<string>();
            var selectedIndex = -1;
            if (!DA.GetData<object>("RadioList to modify", ref listObject)) return;
            DA.GetDataList<string>("New radiolist contents", listContents);

            //Get the scroll viewer
            ScrollViewer sv = HUI_Util.GetUIElement<ScrollViewer>(listObject);
            //Get the itemsControl inside the scrollviewer
            ItemsControl ic = sv.Content as ItemsControl;

            DA.GetData("Selected Index", ref selectedIndex);

            var groupName = Guid.NewGuid().ToString("N");
            if (listContents.Any())
            {
                ic.Items.Clear();
                foreach (var item in listContents)
                {
                    ic.Items.Add(new RadioButton
                    {
                        Content = item,
                        Margin = new System.Windows.Thickness(2),
                        GroupName = groupName
                    });
                }
            }
            if (selectedIndex > -1)
            {
                ((RadioButton)ic.Items[selectedIndex]).IsChecked = true;
            }
        }

        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        // protected override System.Drawing.Bitmap Icon => Properties.Resources.SetCheckList;

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid => new Guid("{BE77A82A-3C61-4A64-8EFB-6B94948EA973}");
    }
}