﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using Grasshopper.Kernel;
using HumanUI.Components.UI_Elements;
using Rhino.Geometry;

namespace HumanUI.Components.UI_Output
{
    /// <summary>
    /// A component to set the contents of an existing Checklist object
    /// </summary>
    /// <seealso cref="Grasshopper.Kernel.GH_Component" />
    public class SetPullDown_Component : GH_Component
    {
        /// <summary>
        /// Initializes a new instance of the SetList_Component class.
        /// </summary>
        public SetPullDown_Component()
            : base("Set Pulldown menu", "SetPulldown",
                "Use this to set the contents of a pulldown menu",
                "Human UI", "UI Output")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Pulldown to modify", "P", "The list object to modify", GH_ParamAccess.item);
            pManager[pManager.AddTextParameter("Label", "L", "Optional label for the Text Box", GH_ParamAccess.item, "")].Optional = true;
            pManager[pManager.AddTextParameter("List Items", "L", "The list of options to display in the list.", GH_ParamAccess.list)].Optional = true;
            pManager[pManager.AddIntegerParameter("Selected Index", "I", "The initially selected index. Defaults to the first item.", GH_ParamAccess.item, 0)].Optional = true;
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            object listObject = null;
            var listContents = new List<string>();
            var isSelected = -1;
            var label = string.Empty;
            if (!DA.GetData<object>("Pulldown to modify", ref listObject)) return;
            DA.GetDataList<string>("List Items", listContents);
            DA.GetData("Label", ref label);

            //Get the dock panel viewer
            var dp = HUI_Util.GetUIElement<DockPanel>(listObject);
            var pd = dp.Children.OfType<ComboBox>().First();
            var lbl = dp.Children.OfType<Label>().First();
            if (listContents.Any())
            {
                pd.Items.Clear();
                foreach (var item in listContents)
                {
                    pd.Items.Add(item);
                }
            }
            if (!string.IsNullOrEmpty(label))
            {
                lbl.Content = label;
            }
            
            DA.GetData<int>("Selected Index", ref isSelected);
            if (isSelected > -1)
            {
                pd.SelectedIndex = isSelected;
            }
        }

        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        // protected override System.Drawing.Bitmap Icon => Properties.Resources.SetCheckList;

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid => new Guid("{D74244E2-FFF2-4915-A936-F487BBAB2379}");
    }
}